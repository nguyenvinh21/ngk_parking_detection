# NGK Entrance and Exit Detection

Source code for creating demo clip

## Setup environment

### Basic requirement
*  GPU

### Create a virtual environment


*  Run script below:

```
conda create -n name_environment python=3.6
conda activate name_environment
```

*  Then checking cuda version by:

```
nvcc --version

```
or 

```
cd /usr/local/

ls

```
![](image/cuda_check.png)

* Installing dependency libraries by running:

With version of CUDA above for example 10.0, we will run: 

```
chmod u+x ./setup.sh
CUDA=10.0 ./setup.sh

```

## Run code for inferencing process:

For avoiding permission error in Linux we will run:

```
chmod u+x ./cmd_test.sh

```

### Parameters:
- **GPUS**: integer, Setting GPU device for inferencing
- **MODEL**: string, Name of model. Default= "efficientnet-b0"
- **DIR**: string, Path to video or webcam 
- **SHOW**: string,default = *"false"*.  *"true"* (show video while inferencing code), *"false"*: turn off displaying mode while inferencing

### Example:

1.  With demo video:

Change the number in GPUS parameter with the GPU card you want to use.

For example: we have a video "test.avi" with global path: /home/vinhng/test.avi, we will run code as:

```
GPUS=0 DIR=/home/vinhng/test.avi ./cmd_test.sh

```
To display video while running code, we just need to add **SHOW=true**:

```
GPUS=0 DIR=/home/vinhng/test.avi SHOW=true ./cmd_test.sh

```
*  Name of all models: **resnet50**, **resnest50**, **tresnet_m**, **efficientnet-b0** 

To change or use other model, for example: **resnet50** , we have:

```
GPUS=0 DIR=/home/vinhng/test.avi MODEL=resnet50 SHOW=true ./cmd_test.sh
```

2.  With webcam:

* DEFAULT:

```
GPUS=0 DIR=0 SHOW=true ./cmd_test.sh
```
* If get error, you need to check existing of Webcam and its global path

We can check existing of device by:
```
ls /dev/video*

```
We assume that global path of webcam is : "/dev/video1", we will run code:

```
GPUS=0 DIR=/dev/video1 SHOW=true ./cmd_test.sh

```
### Demo clip location:

When it's finish, demo video will be located at output_video folder in the same directory:

```
./output_video

```
## Dataset preparation for training process:

*  Vacant Parking Spaces Using Dashcam Videos: 800 clips
    * Train: 560 clips
    * Test: 240 clips
*  NGK'dataset: 250 clips
    * Train: 175 clips
    * Test: 75 clips
* All clips are extracted into frames. The current frame is equidistant 5 frames from previous frame
## Result on NGK' test set:

| Model | GPU | Image size | Batch size | Infer time (ms) | Accuracy (%) |
| ------ | ------ | ------ | ------ | ------ | ------ |
| ResNeSt 50 | RTX 2080ti | 224 | 1 | 21 | 97.28 |
| EfficientNet B0 | RTX 2080ti | 224 | 1 | 19 | 97.34 |
| TresNet M | RTX 2080ti | 224 | 1 | 18 | 97.00 |
| ResNet 50 | RTX 2080ti | 224 | 1 | 12 | 97.32 |
