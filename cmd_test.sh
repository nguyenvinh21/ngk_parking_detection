if [ "${MODEL}" == "" ]; then
    MODEL=efficientnet-b0
fi
CUDA_VISIBLE_DEVICES=${GPUS} python demo.py --video_dir "${DIR}" --model "${MODEL}" --is_show "${SHOW}"