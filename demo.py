import argparse
import json
import os
from src.trainer import NGKTrainer

def main(params):

    # Parse config
    print(params)
    trainer = NGKTrainer(**params)
    trainer.demo()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Cybercore - Entrance and Exit Detection - Testing')
    parser.add_argument('--config', default='./config/train_config.json', type=str, help='config file')
    parser.add_argument('--model', default='efficientnet-b0', type=str, help = "name of model")
    parser.add_argument('--video_dir', type = str, help = 'Path to video location of webcam')
    parser.add_argument('--is_show', type = str, default = "false", help = 'Show video while predicting or not')
    args = parser.parse_args()

    #Checking errors
    if not args.video_dir:
        raise "Please adding the path of video or webcam"

    # if not os.path.exists(args.video_dir):
    #     raise NameError

    params = json.load(open(args.config, 'r'))
    params["demo_input"] = args.video_dir
    if len(params["demo_input"]) == 1:
        params["demo_input"] = int(params["demo_input"])

    params["net_type"] = args.model
    params["is_show"] = args.is_show
    main(params)