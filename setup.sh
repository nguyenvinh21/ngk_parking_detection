export CUDA_HOME=/usr/local/cuda-${CUDA}
export LD_LIBRARY_PATH=${CUDA_HOME}/lib64 
PATH=${CUDA_HOME}/bin:${PATH} 
export PATH

pip install -r requirements.txt

pip install git+https://github.com/rwightman/pytorch-image-models
pip install git+https://github.com/mapillary/inplace_abn.git@v1.0.11