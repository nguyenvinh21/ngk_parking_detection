from torchvision import transforms
import numpy as np 
import cv2 
from PIL import ImageFile, Image 
import torchvision

def inference_transformation(image, size):
    transform = transforms.Compose([
        transforms.Resize((size,size)),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
    ])
    im = transform(image)
    return im