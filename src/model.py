import os
import torch
import math
from torch import nn
import torch.nn.functional as F
from torchvision import models
from efficientnet_pytorch import EfficientNet

def convert_relu_to_swish(model):
    for child_name, child in model.named_children():
        if isinstance(child, nn.ReLU):
            setattr(model, child_name, swish())
        else:
            convert_relu_to_swish(child)
            
def get_torchvision_model(net_type, is_trained, num_classes, loss):
    """ Get torchvision model

    Parameters
    ----------
    net_type: str
        deep network type
    is_trained: boolean
        use pretrained ImageNet
    num_classes: int
        number of classes

    Returns
    -------
    nn.Module
        model based on net_type
    """
   
    if net_type.startswith("efficientnet"):
        return CustomEfficientNet(net_type, is_trained, num_classes, loss)

    elif net_type.startswith("resnest"):
        return CustomResNeSt(net_type, is_trained, num_classes, loss)
    
class CustomEfficientNet(nn.Module):
    def __init__(self, net_type, is_trained, num_classes, loss):
        super().__init__()
        self.net = EfficientNet.from_pretrained(net_type) if is_trained else EfficientNet.from_name(net_type)
        kernel_count = self.net._fc.in_features
        self.net._fc = nn.Linear(kernel_count, num_classes)
        self.sigmoid = nn.Sigmoid()
    def forward(self, x):
        x = self.net(x)
        if not self.training:
             x = self.sigmoid(x)
        return x

class CustomResNeSt(nn.Module):
    def __init__(self, net_type, is_trained, num_classes, loss):
        super().__init__()
        self.net = torch.hub.load('zhanghang1989/ResNeSt', net_type, pretrained= is_trained)
        kernel_count = self.net.fc.in_features
        self.net.fc = nn.Linear(kernel_count, num_classes)
        self.sigmoid = nn.Sigmoid()
    def forward(self, x):
        x = self.net(x)
        if not self.training:
             x = self.sigmoid(x)
        return x
