# common libraries
import os
import sys
import copy
import cv2
import numpy as np
from time import time
from PIL import Image
from collections import OrderedDict

#torch libraries
import timm
import torch
import torch.utils.data as utils
from torch.utils.model_zoo import load_url
from torch.autograd import Variable
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch import nn, optim

# src packages
from src.model import get_torchvision_model
from src.NGK_transform import inference_transformation

class Trainer(object):
    def __init__(self, **args):
        for key in args:
            setattr(self, key, args[key])

class NGKTrainer(Trainer):
    
    def __init__(self, **args):
        super(NGKTrainer, self).__init__(**args)

    def get_training_object(self):

        # Weight of model 
        if self.net_type.startswith("resnest"):
            url = "https://storage.googleapis.com/v-project/resnest50-3b349120.pth"
        elif self.net_type.startswith("efficientnet"):
            url = "https://storage.googleapis.com/v-project/efficientnetb0-7f7dfbfa.pth"
        elif self.net_type.startswith("resnet"):
            url = "https://storage.googleapis.com/v-project/resnet50_NGK_6502c63b.pth"
        elif self.net_type.startswith("tresnet"):
            url = "https://storage.googleapis.com/v-project/tresnet-m_NGK-18b938d4.pth"
        else:
            raise "We don't support your model"

        model = get_torchvision_model(self.net_type, self.pretrained, len(self.classes), self.loss)
        
        ############################### Vinh's model #####################################
        if self.net_type.startswith("efficientnet") or self.net_type.startswith("resnest"):
            model = get_torchvision_model(self.net_type, self.pretrained, len(self.classes), self.loss)

        ############################## Son's model ########################################
        elif self.net_type.startswith("tresnet"):
            model = timm.create_model(self.net_type, num_classes = 2, pretrained = False)
        elif self.net_type.startswith("resnet"):
            model = timm.create_model(self.net_type, num_classes=2, pretrained = False)

        #checking GPU or not
        if torch.cuda.is_available() == False:
            device = "cpu"
            model = model.cpu()

        else:
            device = "cuda"
            model = model.cuda()
            if self.multi_gpu:
                model = nn.DataParallel(model)

        ##################### Loading Weight of model ####################################
        if self.weight_path is not None:
            print("Loading Weight from folder")
            state_dict = torch.load(self.weight_path, map_location = device)
            if self.net_type.startswith("resnest") or self.net_type.startswith("efficientnet"):
                state_dict = state_dict["state"]
            model.load_state_dict(state_dict)

        else:
            state_dict = load_url(url, map_location = device)
            if self.net_type.startswith("resnest") or self.net_type.startswith("efficientnet"):
                state_dict = state_dict["state"]
            model.load_state_dict(state_dict)
        print("Loading weighted model success")

        #### optimizer
        optimizer = torch.optim.Adam(model.parameters(), lr = self.lr, betas=(0.9, 0.999))
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode = "max", factor = self.factor, patience= self.patience)
        
        if self.loss.startswith("lovasz"):
            loss_criteria = binary_xloss(ignore=255)
        elif self.loss.startswith("focal"):
            loss_criteria = FocalLoss(gamma=self.gamma)
        elif self.loss.startswith("smooth"):
            loss_criteria = nn.SmoothL1Loss()
        else:
            loss_criteria = nn.BCEWithLogitsLoss()
 
        return model, loss_criteria, optimizer, scheduler, device
        
    ############################### DEMO ##########################################
    def demo(self):

        # parameters
        labels = {
            0: "IN THE PARKING LOT",
            1: "ON THE ROAD"
        }
        seq_len = self.demo_seq_length
        
        # setting wait variable for visualization:
        checkpoint= False
        wait_var = 0
        y_offset=50
        frames = []
        pred_labels = ""
        speed = 0.
        if isinstance(self.demo_input, int):
            video_name = ""  
        else:
            video_name = self.demo_input.rsplit("/")[-1]

        # Loading model
        model, loss_criteria, optimizer, scheduler, device = self.get_training_object()
        model.eval()
        # model = model.to(device)
        ###################
        ## Reading video ##
        ###################

        frame_provider = cv2.VideoCapture(self.demo_input)
        height = int(frame_provider.get(cv2.CAP_PROP_FRAME_HEIGHT))
        width = int(frame_provider.get(cv2.CAP_PROP_FRAME_WIDTH))

        fps = frame_provider.get(cv2.CAP_PROP_FPS)
        total_frame = frame_provider.get(cv2.CAP_PROP_FRAME_COUNT)

        os.makedirs(self.demo_output, exist_ok= True)

        video_dir = os.path.join(self.demo_output, video_name)

        # Preparing demo clip
        if not video_name == "":
            fourcc = cv2.VideoWriter_fourcc(*'XVID')
            out_video = cv2.VideoWriter(video_dir,fourcc, 25.0, (width,height))

        print("Total frame: ", total_frame)
        print("duration of video: ", float(total_frame)/ float(fps + 1e-6))

        if not frame_provider.isOpened():
            raise IOError("Video {} can not be oppend".format(self.demo_input))

        # Start Reading video
        while(frame_provider.isOpened()):
            
            # wait_var+=1

            ret, frame = frame_provider.read()
            if ret == False:
                break
            if len(frames) != seq_len:
                if self.net_type.startswith("efficientnet") or self.net_type.startswith("resnest"):
                    frame_processed = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                    frame_processed = Image.fromarray(frame_processed)
                    frame_processed = inference_transformation(frame_processed, self.size)
                else:
                    frame_processed = cv2.resize(frame, (224,224))
                    frame_processed = torch.as_tensor(frame_processed).float()
                    frame_processed = frame_processed/225 - 0.5
                    frame_processed = frame_processed.permute(2,0,1)
                frames.append(frame_processed)

            if len(frames) == seq_len:
                start = time()
                frames = torch.stack(frames, dim=0)
                # print(frames.size())
                # feeding input
                index = torch.linspace(0, seq_len - 1, self.demo_sampling_rate).long()
                # print("##############################")
                # print(index)
                inputs = torch.index_select(frames, 0, index)
                if device == "cpu":
                    inputs = inputs.cpu()
                else:
                    inputs = inputs.cuda()

                preds = model(inputs)

                preds = torch.mean(preds, dim=0)
                if self.net_type.startswith("efficientnet") or self.net_type.startswith("resnest"):
                    preds = (preds > self.threshhold) * 1
                    pred_labels = labels[preds.item()]
                else:
                    preds = preds.argmax(-1).cpu()
                    pred_labels = labels[preds.item()]
                # Reset buffer frame
                frames = []
                speed = time() - start
                checkpoint = True
                print("Speed: {:.3f} seconds".format(speed))
        
            ############### DISPLAY ##################
            if checkpoint:
                if len(frames) <= self.demo_seq_length: 
                    if pred_labels == "IN THE PARKING LOT":
                        color = (0, 204, 204)
                    else:
                        color = (0,204,0)
                    cv2.putText(frame, '{}'.format(pred_labels), (20, 40),
                                        fontFace= cv2.FONT_HERSHEY_SIMPLEX,
                                        fontScale= .85, color = color, thickness = 2)

                    cv2.putText(frame, 'Model: {}'.format(self.net_type), (20, 70),
                                        fontFace= cv2.FONT_HERSHEY_SIMPLEX,
                                        fontScale= .65, color = (204, 102, 0), thickness = 2)

                    cv2.putText(frame, 'FPS: {}'.format(round(1/speed)), (20, 100),
                                        fontFace= cv2.FONT_HERSHEY_SIMPLEX,
                                        fontScale= .65, color = (204, 102, 0), thickness = 2)
            
            if self.is_show == "true":
                cv2.imshow("Frame Test", frame)
                if cv2.waitKey(25) & 0xFF == ord('q'):
                    break
            if not video_name == "":
                out_video.write(frame)
        out_video.release()
        frame_provider.release()
            

